<?php 
	session_start();

	if (!isset($_SESSION['Alumno'])) {
		$_SESSION['Alumno'] = [
    1 => [
      'num_cta' => '1',
      'nombre' => 'Admin',
      'p_apellido' => 'General',
      's_apellido' => '',
      'password' => 'adminpass123.',
      'genero' => 'O',
      'fecha_nac' => '1994-02-28'
    ]
  ];
	}
?>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Login</title>
	<link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre.min.css">
    <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre-exp.min.css">
    <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre-icons.min.css">

    <style type="text/css">
    	body{
    		background: whitesmoke;
    		display: flex;
    	}

    	.contenedor{
    		width: 600px;
				height: 400px;
				margin: 10px;
				background: whitesmoke;
				margin: auto;
				border-style: solid;
				border-radius: 25px;
				border-color: #5755d9;
    	}

    	.login{
    		margin: 10px;
    	}
	</style>

</head>
<body>

	<div class="contenedor">
		<center><h3>Login</h3></center>
		<br/>
        <div class="login">
          <form id="form-login" method="POST">
            
            <label class="form-label" for="input-ncuenta">Número de Cuenta:</label>
            <input name="cuenta" class="form-input " type="text" id="input-ncuenta" placeholder="Ingrese su numero de cuenta">
            <br/>

            <label class="form-label" for="input-password">Contraseña:</label>
						<input name="password" class="form-input" type="password" id="input-password" placeholder="Contraseña">
						<br/>

            <input name="enviar" type='submit' class="btn btn-primary" value="Enviar"/>
            <br/>


          </form>
        </div>        
      </div>

</body>
</html>

<?php

	if (isset($_SESSION['login'])) {
		header('Location: info.php');
	}

	if (isset($_POST['enviar'])) {
    $cuenta = $_POST['cuenta'];
    $password = $_POST['password'];

    if(!empty($_SESSION['Alumno'])) {
      foreach($_SESSION['Alumno'] as $llave => $valor){
        if ($valor['num_cta'] == $cuenta and $valor['password'] == $password) {
          $_SESSION['login'] = $cuenta;
          header('Location: info.php');
        } else {
          echo "<script>alert('Número de Cuenta o Contraseña incorrectos. Vuelve a intentarlo') </script>";
        }
      }
    }    
  }
?>

