<?php 
  session_start();

  if (!isset($_SESSION['login'])) {
    header('Location: login.php');
  }
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Formulario</title>
	<link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre.min.css">
    <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre-exp.min.css">
    <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre-icons.min.css">
    <style type="text/css">

    	body{
    		background: whitesmoke;
    	}

    	.navbar{
			background: #5755d9;
    	}

    	.navbar-collapse{
	      display: flex;
	    }

    	#navbtn{
    		border: none;
    	}

    	.container{
    		display: flex;
    	}

    	.formulario{
			width: 90%;
			height: 100%;
			margin-top: 40px;
			margin-left: auto;
			margin-right: auto;
			border-style: solid;
			border-radius: 25px;
			border-color: #5755d9;
    	}

    	.campos{
    		margin: 20px;
    	}

    	footer{
    		margin: 20px;
    	}

    </style>

</head>
<body>

	<nav class="navbar">
	      <div class="navbar-collapse">
	        <button class="btn btn-primary btn-lg" id="navbtn" onclick="location.href='info.php'">Home</button>
	        <button class="btn btn-primary btn-lg" id="navbtn" onclick="location.href='formulario.php'">Registar Alumnos <i class="icon icon-people"></i></button>
	        <form method="POST">
	          <input name="cerrar-sesion" type='submit' class="btn btn-primary btn-lg" id="navbtn" value="Cerar Sesión"/>
	        </form>
	        <?php
	          if (isset($_POST['cerrar-sesion'])) {
	            unset($_SESSION['login']);
	            header('Location: login.php');
	          }
	        ?>
	      </div>
	</nav>

	<main class="container">
		<div class="formulario">
			<div class="campos">

				<form action="" id="form-login" method="POST">
					<label class="form-label" for="input-ncuenta">Número de Cuenta:</label>
	            	<input name="num_cta" class="form-input " type="text" id="input-ncuenta" placeholder="Número de Cuenta">
	            	<br/>
	            	
	            	<label class="form-label" for="input-nombre">Nombre:</label>
	            	<input name="nombre" class="form-input " type="text" id="input-nombre" placeholder="Nombre">
	            	<br/>
	            	
	            	<label class="form-label" for="input-papellido">Primer Apellido:</label>
	            	<input name="p_apellido" class="form-input " type="text" id="input-papellido" placeholder="Primer Apellido">
	            	<br/>
	            	
	            	<label class="form-label" for="input-sapellido">Segundo Apellido:</label>
	            	<input name="s_apellido" class="form-input " type="text" id="input-sapellido" placeholder="Segundo Apellido">
	            	<br/>
	            	
	            	<label class="form-label">Sexo</label>
					<label class="form-radio">
						<input type="radio" name="genero" value="H" checked>
						<i class="form-icon"></i> Hombre
					</label>
					<label class="form-radio">
						<input type="radio" name="genero" value="M">
						<i class="form-icon"></i> Mujer
					</label>
		            <br/>

		            <label class="form-label" for="input-date">Fecha de Nacimiento</label>
					<input name="fecha_nac" class="form-input " type="date" id="input-date" placeholder="Fecha">

					<label class="form-label" for="input-password">Contraseña:</label>
					<input name="password" class="form-input" type="password" id="input-password" placeholder="Contraseña">
					<br/>

					<input name="registrar" type='submit' class="btn btn-primary btn-lg" value="Registar"/>
					<br/>

	            </form>
			</div>
		</div>
	</main>

	<footer>
		
	</footer>

</body>
</html>

<?php
	if (isset($_POST['registrar'])) {
		if ($_POST['num_cta'] == '' or $_POST['password'] == '') {
			echo "<script>alert('El Número de Cuenta o Contraseña no es valido.') </script>";
		}else{
			$valido = true;
			foreach ($_SESSION['Alumno'] as $key => $value) {
				if ($value['num_cta'] == $_POST['num_cta']) {
					$valido = false;
				}
			}
			if ($valido) {
				$registro = [
			      'num_cta' => $_POST['num_cta'],
			      'nombre' => $_POST['nombre'],
			      'p_apellido' => $_POST['p_apellido'],
			      's_apellido' => $_POST['s_apellido'],
			      'password' => $_POST['password'],
			      'genero' => $_POST['genero'],
			      'fecha_nac' => $_POST['fecha_nac']
			    ];
		    	array_push($_SESSION['Alumno'],$registro);
			}else{
				echo "<script>alert('El número de Cuenta ya se encuentra registrado. Intentelo de nuevo.') </script>";
			}
		}    
  }
?>