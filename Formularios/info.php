<?php 
  session_start();

  if (!isset($_SESSION['login'])) {
    header('Location: login.php');
  }
?>

<!DOCTYPE html>
<html>
<head>
  
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Información</title>
  <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre.min.css">
  <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre-exp.min.css">
  <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre-icons.min.css">
  <style type="text/css">

    body{
      background: whitesmoke;
    }

    .navbar{
      background: #5755d9;
    }

    .navbar-collapse{
      display: flex;
    }

    #navbtn{
      border: none;
    }

    .container{
        display: flex;
      }

      .maindiv{
      width: 95%;
      height: 100%;
      margin-top: 40px;
      margin-left: auto;
      margin-right: auto;
      border-style: solid;
      border-radius: 25px;
      border-color: #5755d9;
      }

      .infousuario{
        width: 98%;
        height: 65%;
        background: #5755d9;
        border-style: solid;
        border-radius: 25px;
        border-color: #5755d9;
        margin-top: 10px;
        margin-bottom: 50px;
        margin-right: auto;
        margin-left: auto;
        color: white;
      }

      .infousuario div{
        margin: 10px;
      }

      .infodatos{
        width: 98%;
        height: 45%;
        border-style: solid;
        border-radius: 25px;
        border-color: #5755d9;
        margin-top: 10px;
        margin-bottom: 10px; 
        margin-right: auto;
        margin-left: auto;
      }

      .infodatos div{
        margin: 10px;
      }

      footer{
        margin: 20px;
      }

  </style>  

</head>
  <body>

    <nav class="navbar">
      <div class="navbar-collapse">
        <button class="btn btn-primary btn-lg" id="navbtn" onclick="location.href='info.php'">Home</button>
        <button class="btn btn-primary btn-lg" id="navbtn" onclick="location.href='formulario.php'">Registar Alumnos <i class="icon icon-people"></i></button>
        <form method="POST">
          <input name="cerrar-sesion" type='submit' class="btn btn-primary btn-lg" id="navbtn" value="Cerar Sesión"/>
        </form>
        <?php
          if (isset($_POST['cerrar-sesion'])) {
            unset($_SESSION['login']);
            header('Location: login.php');
          }
        ?>
      </div>
    </nav>

    <main class="container">
    <div class="maindiv">
      <div class="infousuario">
        <div>
          <h2>Usuario Autenticado</h2>
          <figure class="avatar avatar-xl">
            <img src="https://i.pravatar.cc/128" alt="...">
          </figure>
          <?php
            foreach($_SESSION['Alumno'] as $llave => $valor){
              if ($valor['num_cta'] == $_SESSION['login']) {
                echo "<b>" . $valor['nombre'] . "</b><br><br>";
                echo "<u>Información:</u><br>";
                echo "Número de Cuenta: <i>" . $valor['num_cta'] . "</i><br>";
                echo "Fecha de nacimiento: <i>" . $valor['fecha_nac'] . "</i><br>";
              }
            } 
          ?>
        </div>
      </div>

      <div class="infodatos">
        <div>
          <h3>Datos guardados</h3>
          <table class="table">
            <thead>
              <tr>
                <th>#</th>
                <th>Nombre</th>
                <th>Fecha de Nacimiento</th>
              </tr>
            </thead>
            <tbody>
              <?php
                foreach($_SESSION['Alumno'] as $llave => $valor){
                  echo "<tr> <td>" . $llave . "</td><td>" . $valor['nombre'] . "</td><td>" . $valor['fecha_nac'] . "</td></tr>";
                }
              ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </main>

  <footer>
    
  </footer>

  </body>
</html>
