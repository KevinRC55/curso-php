# Formularios

## Objetivo

Crear un sitio web que contenga las siguientes páginas:

* Una página  para ingresar llamada login.php.
* Una página donde se muestre la información del usuario que se autenticó y abajo toda la información guardada en sesión llamada info.php.
* Una página donde se capture y guarde en sesión la información de alumnos formulario.php.

### Consideraciones:

* Toda la información capturada en el formulario se tiene que guardar en sesión.
* No se podrá acceder a la página de info.php o de formulario.php si no se ha iniciado sesión.
* Las páginas de info y formulario deben tener ligas para poder navegar entre las páginas y poder cerrar sesión.
* Ya que no se puede acceder sin un usuario se tendrá un usuario pre cargado en el sistema con los siguientes datos:
	* Número de cuenta: 1
	* Nombre: Admin
	* Primer Apellido: General
	* Contraseña: adminpass123.
	* Género: Otro

![](./captura_login.png)

![](./captura_formulario.png)

![](./captura_info.png)
