<html>
<head>
	<meta charset="utf-8">
	<title>Pirámide y rombo</title>
	<style type="text/css">
		body{
			font-family: Impact, fantasy;
			color: steelblue;
		}
	</style>
</head>
<body>

	<?php
		echo "<center>";
		echo "<h1>Pirámide y rombo</h1>";
		for ($i=1; $i <= 30; $i++) { 
			echo str_repeat("#",$i);
			echo "<br/>";
		};
		echo "<br/>";
		for ($i=0; $i <= 30; $i++) { 
			echo str_repeat("#",$i);
			echo "<br/>";
		};
		for ($i=29; $i > 0; $i--) { 
			echo str_repeat("#",$i);
			echo "<br/>";
		};
	?>

</body>
</html>
