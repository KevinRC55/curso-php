<?php

	/*$cadena = "012345678901234567890123456789 0123456789 555555555";

	$sustitucion = 'Este es el ultimo patron: ${1}';

	$result = preg_replace('/^[^\s]{1,30}(\s+[^\s]{1,30})*$/i', $sustitucion, $cadena);

	echo $result;*/

	//Realizar una expresión regular que detecte emails correctos.
	echo "<h2>Emails correctos</h2>";

	echo "Expresion: <b>^[a-z,\.,_]+@[a-z]+\.[a-z]+</b><br>";

	$email = ["ejemplocorrecto@dominio.com",
	"ejemplo_correcto@dominio.com",
	"ejemplo.correcto@dominio.com", 
	"ejemplo incorrecto@dominio.com",
	"ejemplo_incorrecto#dominio.com",
	"ejemplo_incorrecto@dominio_com",
	"ejemplo_incorrecto@dom_inio.com"];

	foreach ($email as $key => $value) {
		if (preg_match('/^[a-z,\.,_]+@[a-z]+\.[a-z]+/i',$value)) {
			echo "<br> El correo <b>$value</b> es <b>correcto</b>";
		}else{
			echo "<br> El correo <b>$value</b> es <b>incorrecto</b>";
		}
	}

	//Realizar una expresion regular que detecte Curps Correctos

	echo "<h2>Curps correctos</h2>";

	echo "Expresion: <b>^([A-Z]{4}[0-9]{6}[H,M][A-Z]{2}[A-Z]{3}[A-Z,0-9][0-9])</b><br>";
	echo "<br>";

	$curp = "ROCE000131HNLDNTA0";

	if (preg_match('/^([A-Z]{4}[0-9]{6}[H,M][A-Z]{2}[A-Z]{3}[A-Z,0-9][0-9])/i', $curp)) {
		echo "El curp <b>$curp</b> tiene un formato correcto";
	}

	//Realizar una expresion regular que detecte palabras de longitud mayor a 50
	//formadas solo por letras.

	echo "<h2>palabras de longitud mayor a 50</h2>";

	echo "Expresion: <b>^([a-zA-Z]{51,}$)</b><br>";
	echo "<br>";

	$palabra = ["cjcqncqenoiqecnqcnncqoinqcknclkancncqncqncwqncqwncqnqcwkkpcqkncwq", "holamundo"];

	foreach ($palabra as $key => $value) {
		if (preg_match('/^([a-zA-Z]{51,}$)/i', $value)) {
			echo "<br> La palabra: <b>$value</b> tiene mas de 50 letras";
		}else{
			echo "<br> La palabra: <b>$value</b> tiene menos de 50 letras";
		}
	}


	//Crea una funcion para escapar los simbolos especiales.

	echo "<h2>Escapar los simbolos especiales</h2>";

	$cadena = "a!g-f+h^k?";

	function escapar($cadena){
		$result = preg_replace('/([\.\\\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:\-])+/i', "\\", $cadena);
		echo $result;
	}

	escapar($cadena);

	//Crear una expresion regular para detectar números decimales.

	echo "<h2>Detectar números decimales</h2>";

	echo "Expresion: <b>^([0-9]+\.?[0-9]*|\.[0-9]+)$</b><br>";
	echo "<br>";

	$decimal = ["5","6.8","1999","5876.2547841554544","0",".56","98.0","846.565.444","XIV","024AB6B"];

	foreach ($decimal as $key => $value) {
		if (preg_match('/^([0-9]+\.?[0-9]*|\.[0-9]+)$/i', $value)) {
			echo "<br><b>$value</b> es un numero decimal";
		}else{
			echo "<br><b>$value</b> no es un numero decimal";
		}
	}


?>